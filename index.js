const express = require('express')
const bodyParser = require('body-parser')
const cookieparser = require('cookie-parser')
const session = require('express-session')
const path = require('path')

const app = express()
app.use(bodyParser.json())
app.use(cookieparser())
app.use(express.static('dist'))

app.use(session({secret: 'Dinhnhatmai'}))

app.get('/', function (req, res) {
    res.sendFile(path.resolve(path.join(__dirname, 'dist/index.html')))
})

app.get('/api', function (req, res) {
    res.json({data: [], message: null, code: 200, _v: '1.0.0'})
})

app.listen(2001, '172.26.10.56', function() {
    console.log('server running on port 2001 ...')
})